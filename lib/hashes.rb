# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  result = {}
  str.split(" ").each {|word| result[word] = word.length}
  result
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.key(hash.values.max)
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  older.merge!(newer)
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  result = {}
  word.split("").each {|letter| result[letter] = word.count(letter)}
  result
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  result = {}
  arr.select {|item| result[item] = arr.count(item) < 2}
  result.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  result = {:even => 0, :odd => 0}
  numbers.each do |num| 
    num % 2 == 1 ? result[:odd] += 1: result[:even] += 1
  end
  result
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  result = {}
  string.split("").sort.each {|letter| result[letter] = string.count(letter)}
  result.key(result.values.max)
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6 ",Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  late_b_days = all_late_b_days(students)
  result = []
  used = []
  late_b_days.keys.each_with_index do |student1, idx1|
    pairings = [student1] 
    late_b_days.keys.each_with_index do |student2, idx2|
      if student1 != student2 && used.last != [student1, student2].sort
        pairings << student2
        used << [student1, student2].sort
        break
      end
    end
    result << pairings.sort
  end
  result.sort
end

def late_b_day?(month)
  month > 6 ? true: false
end

#returns a hash of all students with late b_days
def all_late_b_days(students)
  result = students
  students.each {|student, month| result.delete(student) unless late_b_day?(month)}
  result
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  number_of_species = specimens.uniq.length
  populations = {}
  specimens.each {|species| populations[species] = specimens.count(species)}
  smallest_population_size = populations.values.min
  largest_population_size = populations.values.max
  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  character_count(normal_sign).each do |letter, num_of_letters|
    if character_count(vandalized_sign)[letter] != nil
      if character_count(vandalized_sign)[letter] > num_of_letters
        return false
      end
    end
  end
  true
end

def character_count(str)
  char_count = {}
  str.split("").each {|letter| char_count[letter] = str.count(letter)}
  char_count
end
